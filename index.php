<?php
require_once ("animal.php");
require_once ("frog.php");
require_once ("ape.php");

$sheep = new Animal("shaun");

echo "Nama: $sheep->name <br>"; // "shaun"
echo "Legs: $sheep->legs <br>"; // 2
echo "Cold blooded: $sheep->cold_blooded <br><br>"; // false


$kodok = new Frog("buduk");
echo "Nama: $kodok->name <br>"; 
echo "Legs: $kodok->legs <br>"; 
echo "Cold blooded: $kodok->cold_blooded <br><br>"; 

$sungokong = new Ape("kera sakti");
echo "Nama: $sungokong->name <br>"; 
echo "Legs: $sungokong->legs <br>"; 
echo "Cold blooded: $sungokong->cold_blooded <br><br>"; 

?>